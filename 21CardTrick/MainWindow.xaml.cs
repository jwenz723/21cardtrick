﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Media.Animation;
using System.Drawing;
using System.Media;
using System.Reflection;

namespace _21CardTrick
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Properties

        /// <summary>
        /// An object to represent the deck that is being used
        /// </summary>
        Deck deck;

        /// <summary>
        /// An object to represent the player
        /// </summary>
        Player player;

        /// <summary>
        /// An object to represent the dealer
        /// </summary>
        Dealer dealer;

        /// <summary>
        /// An image object used for loading all the card images
        /// </summary>
        BitmapImage[,] cardImages = new BitmapImage[3, 7];

        /// <summary>
        /// A thread to handle the splash screen updating
        /// </summary>
        Thread backgroundThread;

        #endregion

        #region Starting/Ending Program Code
        /// <summary>
        /// Constructor
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();

            // initialize all the stuff
            deck = new Deck();
            player = new Player();
            dealer = new Dealer(deck);
            dealer.deal();

            // create a thread to perform splash screen functions
            backgroundThread = new Thread(new ThreadStart(splashPage));
            backgroundThread.Start();
        }

        /// <summary>
        /// This function is called when the default close button 'X' is clicked
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            base.OnClosing(e);

            // kill the background thread.
            // this is needed because the thread will keep running even when the application
            // is closed if the thread has not already been killed previously in the app.
            backgroundThread.Abort();

        }

        /// <summary>
        /// A function to create a nice dynamically updated menu screen that has moving cards
        /// </summary>
        private void splashPage()
        {
            try
            {
                this.Dispatcher.Invoke((Action)(() =>
                    {
                        dealer.deal();
                        refreshCards();

                    }));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            Thread.Sleep(2000);
            splashPage();
        }

        #endregion

        #region Other Methods

        /// <summary>
        /// When the start game (restart game) button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnStartTrick_Click(object sender, RoutedEventArgs e)
        {
            try {
                backgroundThread.Abort();
                dealer.deal();
                refreshCards();

                // Play the card dealing sound
                playDealSound();
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception
                HandleError(MethodInfo.GetCurrentMethod().DeclaringType.Name,
                            MethodInfo.GetCurrentMethod().Name, ex.Message);
            }
        }

        /// <summary>
        /// This function will update the cards on the screen
        /// </summary>
        public void refreshCards()
        {
            try
            {
                for (int c = 0; c < 3; c++)
                {
                    for (int r = 0; r < 7; r++)
                    {
                        try
                        {
                            cardImages[c, r] = new BitmapImage(new Uri(dealer.trickCards[c, r].imagePath, UriKind.RelativeOrAbsolute));
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }
                    }
                }

                //Set column 1 card images
                c0r0.Source = cardImages[0, 0];
                c0r1.Source = cardImages[0, 1];
                c0r2.Source = cardImages[0, 2];
                c0r3.Source = cardImages[0, 3];
                c0r4.Source = cardImages[0, 4];
                c0r5.Source = cardImages[0, 5];
                c0r6.Source = cardImages[0, 6];

                //Set column 2 card images
                c1r0.Source = cardImages[1, 0];
                c1r1.Source = cardImages[1, 1];
                c1r2.Source = cardImages[1, 2];
                c1r3.Source = cardImages[1, 3];
                c1r4.Source = cardImages[1, 4];
                c1r5.Source = cardImages[1, 5];
                c1r6.Source = cardImages[1, 6];

                //Set column 3 card images
                c2r0.Source = cardImages[2, 0];
                c2r1.Source = cardImages[2, 1];
                c2r2.Source = cardImages[2, 2];
                c2r3.Source = cardImages[2, 3];
                c2r4.Source = cardImages[2, 4];
                c2r5.Source = cardImages[2, 5];
                c2r6.Source = cardImages[2, 6];
            }
            catch (Exception ex)
            {
                //Just throw the exception
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// This function performs actions that need to occur when the player's
        /// card has been found.
        /// </summary>
        private void cardIsFound()
        {
            try
            {
                // clear out the deal number
                dealer.dealNumber = 0;

                // animate all the cards off of the screen except the player's selected
                // card. And display a label to tell the user that the dealer found the card.
                Storyboard sb = this.FindResource("CardFound") as Storyboard;
                sb.Begin();

                // Create new SoundPlayer in the using statement.
                using (SoundPlayer player = new SoundPlayer(@"../../Assets/tada.wav"))
                {
                    player.Play();
                }
            }
            catch (Exception ex)
            {
                //Just throw the exception
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// This function will play the card dealing sound
        /// </summary>
        private void playDealSound()
        {
            try
            {
                // Create new SoundPlayer in the using statement.
                using (SoundPlayer player = new SoundPlayer(@"../../Assets/cards.wav"))
                {
                    player.Play();
                }
            }
            catch (Exception ex)
            {
                //Just throw the exception
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        #endregion

        #region Column Glow Functions
        //Column Glow Animations *************************************************************************************************
        //************************************************************************************************************************
        private void col1Glow_MouseEnter(object sender, MouseEventArgs e)
        {
            try
            {
                Rectangle col1Glow = (Rectangle)sender;
                DoubleAnimation animation = new DoubleAnimation(1, TimeSpan.FromSeconds(0.2));
                col1Glow.BeginAnimation(Rectangle.OpacityProperty, animation);
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception
                HandleError(MethodInfo.GetCurrentMethod().DeclaringType.Name,
                            MethodInfo.GetCurrentMethod().Name, ex.Message);
            }
        }

        private void col1Glow_MouseLeave(object sender, MouseEventArgs e)
        {
            try
            {
                Rectangle glow = (Rectangle)sender;
                DoubleAnimation animationOut = new DoubleAnimation(0, TimeSpan.FromSeconds(0.2));
                glow.BeginAnimation(Rectangle.OpacityProperty, animationOut);
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception
                HandleError(MethodInfo.GetCurrentMethod().DeclaringType.Name,
                            MethodInfo.GetCurrentMethod().Name, ex.Message);
            }
        }

        private void col2Glow_MouseEnter(object sender, MouseEventArgs e)
        {
            try
            {
                Rectangle col1Glow = (Rectangle)sender;
                DoubleAnimation animation = new DoubleAnimation(1, TimeSpan.FromSeconds(0.2));
                col1Glow.BeginAnimation(Rectangle.OpacityProperty, animation);
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception
                HandleError(MethodInfo.GetCurrentMethod().DeclaringType.Name,
                            MethodInfo.GetCurrentMethod().Name, ex.Message);
            }
        }

        private void col2Glow_MouseLeave(object sender, MouseEventArgs e)
        {
            try
            {
                Rectangle glow = (Rectangle)sender;
                DoubleAnimation animationOut = new DoubleAnimation(0, TimeSpan.FromSeconds(0.2));
                glow.BeginAnimation(Rectangle.OpacityProperty, animationOut);
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception
                HandleError(MethodInfo.GetCurrentMethod().DeclaringType.Name,
                            MethodInfo.GetCurrentMethod().Name, ex.Message);
            }
        }

        private void col3Glow_MouseEnter(object sender, MouseEventArgs e)
        {
            try
            {
                Rectangle col1Glow = (Rectangle)sender;
                DoubleAnimation animation = new DoubleAnimation(1, TimeSpan.FromSeconds(0.2));
                col1Glow.BeginAnimation(Rectangle.OpacityProperty, animation);
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception
                HandleError(MethodInfo.GetCurrentMethod().DeclaringType.Name,
                            MethodInfo.GetCurrentMethod().Name, ex.Message);
            }
        }

        private void col3Glow_MouseLeave(object sender, MouseEventArgs e)
        {
            try
            {
                Rectangle glow = (Rectangle)sender;
                DoubleAnimation animationOut = new DoubleAnimation(0, TimeSpan.FromSeconds(0.2));
                glow.BeginAnimation(Rectangle.OpacityProperty, animationOut);
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception
                HandleError(MethodInfo.GetCurrentMethod().DeclaringType.Name,
                            MethodInfo.GetCurrentMethod().Name, ex.Message);
            }
        }
        
        /// <summary>
        /// When column 1 is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void col1Glow_MouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                Rectangle glow = (Rectangle)sender;
                DoubleAnimation animationOut = new DoubleAnimation(0, TimeSpan.FromSeconds(0.2));
                glow.BeginAnimation(Rectangle.OpacityProperty, animationOut);

                dealer.collect(1);

                // Check if the player's card has been found and deal the cards
                if (dealer.isPlayerCardFound())
                {
                    cardIsFound();
                }
                else
                {
                    // play the sound effect
                    playDealSound();
                }

                refreshCards();
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception
                HandleError(MethodInfo.GetCurrentMethod().DeclaringType.Name,
                            MethodInfo.GetCurrentMethod().Name, ex.Message);
            }
        }

        /// <summary>
        /// When column 2 is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void col2Glow_MouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                Rectangle glow = (Rectangle)sender;
                DoubleAnimation animationOut = new DoubleAnimation(0, TimeSpan.FromSeconds(0.2));
                glow.BeginAnimation(Rectangle.OpacityProperty, animationOut);

                dealer.collect(2);

                // Check if the player's card has been found and deal the cards
                if (dealer.isPlayerCardFound())
                {
                    cardIsFound();
                }
                else
                {
                    // play the sound effect
                    playDealSound();
                }

                refreshCards();
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception
                HandleError(MethodInfo.GetCurrentMethod().DeclaringType.Name,
                            MethodInfo.GetCurrentMethod().Name, ex.Message);
            }
        }

        /// <summary>
        /// When column 3 is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void col3Glow_MouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                Rectangle glow = (Rectangle)sender;
                DoubleAnimation animationOut = new DoubleAnimation(0, TimeSpan.FromSeconds(0.2));
                glow.BeginAnimation(Rectangle.OpacityProperty, animationOut);

                dealer.collect(3);

                // Check if the player's card has been found and deal the cards
                if (dealer.isPlayerCardFound())
                {
                    cardIsFound();
                }
                else
                {
                    // play the sound effect
                    playDealSound();
                }

                refreshCards();
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception
                HandleError(MethodInfo.GetCurrentMethod().DeclaringType.Name,
                            MethodInfo.GetCurrentMethod().Name, ex.Message);
            }
        }

        //END Column Glow Animations *************************************************************************************************
        //************************************************************************************************************************
        #endregion

        #region Error handler method
        /// <summary>
        /// Handles an error
        /// </summary>
        /// <param name="sClass">The class in which the error occurred</param>
        /// <param name="sMethod">The method in which the error occurred</param>
        private void HandleError(string sClass, string sMethod, string sMessage)
        {
            try
            {
                MessageBox.Show(sClass + "." + sMethod + " -> " + sMessage);
            } //end try

            catch (Exception ex)
            {
                System.IO.File.AppendAllText("C:\\Error.txt", Environment.NewLine +
                                             "HandleError Exception: " + ex.Message);
            }
        }
        #endregion
    }
}
