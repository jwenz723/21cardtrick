﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace _21CardTrick
{
    public enum Suits
    {
        Hearts,
        Diamonds,
        Spades,
        Clubs
    }

        public class Card
        {
            #region Properties
            /// <summary>
            /// The number of the card.  Cards 2 through 10 will be stored with the actual number
            /// Jack = 11
            /// Queen = 12
            /// King = 13
            /// Ace = 1
            /// </summary>
            public int faceValue { get; private set; }

            /// <summary>
            /// The suit of the card.
            /// Heart = 0
            /// Diamond = 1
            /// Spade = 2
            /// Club = 3
            /// </summary>
            public Suits suit { get; private set; }

            /// <summary>
            /// The number value of the card.  King = 10, Queen = 10, etc.
            /// Ace = 11 by default, but can be changed to 1 if needed.
            /// </summary>
            public int cardWorth { get; set; }

            /// <summary>
            /// Stores a string reference to the image that represents the suit of the card
            /// </summary>
            public string imagePath { get; private set; }

            public string displayCharacter { get; private set; }
            #endregion

            #region Functions
            /// <summary>
            /// Constructor that will set the face value and suit of the new card
            /// </summary>
            /// <param name="newFaceValue">Value to set the card's face value to</param>
            /// <param name="newSuit">Value to set the card's suit to.  Example: Suits.Hearts or Suits.Clubs</param>
            public Card(int newFaceValue, Suits newSuit)
            {
                try
                {
                    faceValue = newFaceValue;
                    suit = newSuit;

                    // if card is a 2,3,4,5,6,7,8,9, or 10
                    if (faceValue <= 10)
                    {
                        cardWorth = faceValue;
                    }
                    // if card is jack, queen, or king
                    else if (faceValue > 10 && faceValue < 14)
                    {
                        cardWorth = 10;
                    }
                    // if is ace

                    if (faceValue == 1)
                    {
                        cardWorth = 11;
                    }

                    switch (faceValue)
                    {
                        case 1:
                            displayCharacter = "A";
                            break;
                        case 11:
                            displayCharacter = "J";
                            break;
                        case 12:
                            displayCharacter = "Q";
                            break;
                        case 13:
                            displayCharacter = "K";
                            break;
                        default:
                            displayCharacter = faceValue.ToString();
                            break;
                    }

                    switch (newSuit)
                    {
                        case Suits.Hearts:
                            imagePath = "Assets/h" + displayCharacter.ToLower() + ".png";
                            break;
                        case Suits.Diamonds:
                            imagePath = "Assets/d" + displayCharacter.ToLower() + ".png";
                            break;
                        case Suits.Spades:
                            imagePath = "Assets/s" + displayCharacter.ToLower() + ".png";
                            break;
                        case Suits.Clubs:
                            imagePath = "Assets/c" + displayCharacter.ToLower() + ".png";
                            break;
                    }

                }
                catch (Exception ex)
                {
                    //Just throw the exception
                    throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                        MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
                }
            }

            //Default constructor.
            public Card() { }
            #endregion
        }
    }
