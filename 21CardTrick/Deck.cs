﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace _21CardTrick
{
    class Deck
    {
        #region Properties
        /// <summary>
        /// Contains the 52 cards.  I used a stack because when playing black jack you
        /// should only have access to the card on the top of the deck. This is how stacks are.
        /// </summary>
        private Stack<Card> cards;

        /// <summary>
        /// Contains all the cards in default order (Hearts, Diamonds, Spades, then Clubs).
        /// This data structure is used for the shuffle function.  This structure should not
        /// be used as the actual usable "Deck"
        /// </summary>
        private Card[] unusableCards;
        #endregion

        #region Functions
        /// <summary>
        /// Default Constructor. Creates a deck that contains 52 cards in order.
        /// </summary>
        public Deck()
        {
            try
            {
                // Instantiate the deck
                cards = new Stack<Card>();

                // Instantiate the unusableCards (The default unshuffled deck)
                unusableCards = new Card[52];

                // load in the cards into unusableCards (The default unshuffled deck)
                for (int i = 0; i < 13; i++)
                {
                    // create new cards
                    Card newHeart = new Card(i + 1, Suits.Hearts);
                    Card newDiamond = new Card(i + 1, Suits.Diamonds);
                    Card newSpade = new Card(i + 1, Suits.Spades);
                    Card newClub = new Card(i + 1, Suits.Clubs);

                    // Add the cards to the unusable deck (default unshuffled deck)
                    unusableCards[i] = newHeart;
                    unusableCards[i + 13] = newDiamond;
                    unusableCards[i + 26] = newSpade;
                    unusableCards[i + 39] = newClub;
                }

                // Shuffle the deck 4 times
                shuffleDeck(4);
            }
            catch (Exception ex)
            {
                //Just throw the exception
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// This function returns the card that is on the top of the deck
        /// </summary>
        /// <returns></returns>

        /// <summary>
        /// Shuffles the cards and places them into cards (the stack representing the shuffled deck).
        /// </summary>
        /// <param name="numTimesToShuffle">How many times do you want the deck to be shuffled? (Default is 1)</param>
        public void shuffleDeck(int numTimesToShuffle = 1)
        {
            try
            {
                // define a randomizer
                Random rand = new Random();

                // Copy the default deck
                Card[] newDeck = unusableCards;

                // Loop through however many times the parameter is set to
                for (int i = 0; i < numTimesToShuffle; i++)
                {
                    // loop through for each object contained in newDeck (52 times)
                    for (int n = newDeck.Length - 1; n > 0; --n)
                    {
                        // Swap the current card with a random card in the deck
                        int k = rand.Next(n + 1);
                        Card temp = newDeck[n];
                        newDeck[n] = newDeck[k];
                        newDeck[k] = temp;
                    }
                }

                // Clear out the deck
                cards.Clear();

                // push the newly shuffled deck into cards (stack)
                foreach (Card pushCard in newDeck)
                {
                    cards.Push(pushCard);
                }
            }
            catch (Exception ex)
            {
                //Just throw the exception
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// Picks the first 21 cards off of the top of the shuffled deck and returns them as an array.
        /// </summary>
        /// <returns></returns>
        public Card[,] random21()
        {
            try
            {
                // create a temp array of cards
                Card[,] trickDeck = new Card[3, 7];

                //Pops off the top card from the deck and adds it to the array.
                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 7; j++)
                    {
                        // populate the temp array of cards
                        trickDeck[i,j] = cards.Pop();    
                    }
                }

                //returns the array of 21 random cards.
                return trickDeck;
            }
            catch (Exception ex)
            {
                //Just throw the exception
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }
        #endregion
    }
}
