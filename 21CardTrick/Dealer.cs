﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace _21CardTrick
{
    class Dealer
    {
        /// <summary>
        /// Number of times the dealer has dealt the cards.
        /// </summary>
        public int dealNumber { get; set; }

        /// <summary>
        /// an array to store the 21 cards being used in the magic trick.
        /// </summary>
        public Card[,] trickCards;

        //The possible cards the player could have picked
        private List<Card> possibleCardSelection;

        /// <summary>
        /// The constructor for the Dealer. Accepts a deck, which allows the dealer access to the random21 cards for the array.
        /// </summary>
        /// <param name="deck"></param>
        public Dealer(Deck deck)
        {
            try
            {
                dealNumber = 0;
                trickCards = deck.random21();

                possibleCardSelection = new List<Card>();
            }
            catch (Exception ex)
            {
                //Just throw the exception
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// Deals the cards out for the player to see. First deal merely copies the array. Two through Four 
        /// "pick up" the players selected column second by assigning those cards to the middle 7 items in the array.
        /// </summary>
        /// <param name="columnNumber"></param>
        /// <returns></returns>
        
        public void deal()
        {
            Card[,] nextCardSet = new Card[3, 7];
            for (int i = 0; i < 3; i++)
            {
                for (int k = 0; k < 7; k++)
                {
                    nextCardSet[i, k] = new Card();
                }
            }

            //1-7
            nextCardSet[0,0] = trickCards[0,0];
            nextCardSet[1,0] = trickCards[0,1];
            nextCardSet[2,0] = trickCards[0,2];
            nextCardSet[0,1] = trickCards[0,3];
            nextCardSet[1,1] = trickCards[0,4];
            nextCardSet[2,1] = trickCards[0,5];
            nextCardSet[0,2] = trickCards[0,6];

            //8-14
            nextCardSet[1, 2] = trickCards[1, 0];
            nextCardSet[2, 2] = trickCards[1, 1];
            nextCardSet[0, 3] = trickCards[1, 2];
            nextCardSet[1, 3] = trickCards[1, 3];
            nextCardSet[2, 3] = trickCards[1, 4];
            nextCardSet[0, 4] = trickCards[1, 5];
            nextCardSet[1, 4] = trickCards[1, 6];

            //15-21
            nextCardSet[2, 4] = trickCards[2, 0];
            nextCardSet[0, 5] = trickCards[2, 1];
            nextCardSet[1, 5] = trickCards[2, 2];
            nextCardSet[2, 5] = trickCards[2, 3];
            nextCardSet[0, 6] = trickCards[2, 4];
            nextCardSet[1, 6] = trickCards[2, 5];
            nextCardSet[2, 6] = trickCards[2, 6];


            trickCards = nextCardSet;
            //dealNumber++;
        }

        /// <summary>
        /// Collects the cards after a player has chosen a column. This sorts them in the proper order by storing the chosen 
        /// column into the middle of the stack of cards.
        /// </summary>
        /// <param name="columnNumber"></param>
        public void collect(int columnNumber = 0)
        {


            try
            {
                //Begins Card Collection from board
                //---------------------------------------------------------------------------------
                Card[] chosenColumnCards = new Card[7];
                for (int i = 0; i < 7; i++)
                {
                    chosenColumnCards[i] = new Card();
                }

                Card[,] nextCardSet = new Card[3, 7];
                for (int i = 0; i < 3; i++)
                {
                    for (int k = 0; k < 7; k++)
                    {
                        nextCardSet[i, k] = new Card();
                    }
                }

                    if(columnNumber == 1)
                    {
                        for(int i = 0; i < 7; i++)
                        {
                            nextCardSet[1, i] = trickCards[0, i];
                            chosenColumnCards[i] = trickCards[0, i];
                        }

                        for (int i = 0; i < 7; i++)
                        {
                            nextCardSet[0, i] = trickCards[1, i];
                        }

                        for (int i = 0; i < 7; i++)
                        {
                            nextCardSet[2, i] = trickCards[2, i];
                        }
                    }
                    else if(columnNumber == 2)
                    {
                        for (int i = 0; i < 7; i++)
                        {
                            nextCardSet[1, i] = trickCards[1, i];
                            chosenColumnCards[i] = trickCards[1, i];
                        }

                        for (int i = 0; i < 7; i++)
                        {
                            nextCardSet[0, i] = trickCards[0, i];
                        }

                        for (int i = 0; i < 7; i++)
                        {
                            nextCardSet[2, i] = trickCards[2, i];
                        }
                    }
                    else if(columnNumber == 3)
                    {
                        for (int i = 0; i < 7; i++)
                        {
                            nextCardSet[1, i] = trickCards[2, i];
                            chosenColumnCards[i] = trickCards[2, i];
                        }

                        for (int i = 0; i < 7; i++)
                        {
                            nextCardSet[0, i] = trickCards[0, i];
                        }

                        for (int i = 0; i < 7; i++)
                        {
                            nextCardSet[2, i] = trickCards[1, i];
                        }
                    }
                    
                    //Udates the list of possible cards the player could have chosen.
                    updatePotentialCards(chosenColumnCards);

                    trickCards = nextCardSet;
               


            }
            catch (Exception ex)
            {
                //Just throw the exception
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// Updates the list of cards that the player potentially chose. Called at the end of deal.
        /// </summary>
        /// <param name="columnChosen"></param>
        public void updatePotentialCards(Card[] columnChosen)
        {
            try
            {
                //Creates a temporary list to hold the next card set.
                List<Card> tempCardList = new List<Card>();

                //If this is the first time a player chooses a column, all of the cards are added to the list.
                if(dealNumber == 1)
                {
                    foreach(Card c in columnChosen)
                    {
                        tempCardList.Add(c);
                    }
                }
                //Otherwise, when the next set is passed in, if there is a match between the original two, it is saved. The new list overwrites the old with updated cards.
                else
                {
                    foreach(Card c in columnChosen)
                    {
                        if(possibleCardSelection.Contains(c))
                        {
                            tempCardList.Add(c);
                        }
                    }
                }

                possibleCardSelection = tempCardList;

            }
            catch (Exception ex)
            {
                //Just throw the exception
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }

        }
        //Checks the middle 7 cards of the array and compares it to the secret card the player selected. If 
        //the correct card is found, it returns that card. Otherwise a null card is returned.
        public Card revealCard(Card playerCard)
        {
            try
            {

                Card returnCard = new Card();

                //for (int i = 7; i < 14; i++)
                //{
                //    if (trickCards[i] == playerCard)
                //    {
                //        returnCard = trickCards[i];
                //    }
                //}

                return returnCard;
            }
            catch (Exception ex)
            {
                //Just throw the exception
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// This function is called each time the user clicks on one of the columns.
        /// It will collect the cards, deal the cards, and increment the dealNumber variable.
        /// 
        /// </summary>
        /// <returns>Returns true if the player's card is found (the player has chosen 3 columns).</returns>
        public bool isPlayerCardFound()
        {
            deal();

            dealNumber++;

            if (dealNumber == 3)
            {
                return true;
            }

            return false;
        }
    }
}
